%
% Este codigo es para cambiarle la apareciencia.
%

ghost(Ghost, discarded_path, S, _, Color):- path(S), ghost(Ghost, path, S, _, Color), !.
ghost(pinky, path, S, _, [fg(255,182,193)]):- path(S), !.
ghost(blinky, path, S, _, [fg(red)]):- path(S), !.
ghost(inky, path, S, _, [fg(cyan)]):- path(S), !.
ghost(clyde, path, S, _, [fg(255,165,0)]):- path(S), !.
ghost(_, path, Simbolo, _, [fg(110, 110, 110)]):- path(Simbolo).

% Son los diversos temas que se usan> tema1, tema2 y tema3

maze_writer_for([ color(ColorChooser) | Opciones ], color_write(ColorChooser, PreviousWriter)):-
    maze_writer_for(Opciones, PreviousWriter).
maze_writer_for([ torcido | Opciones ], torcido_write(PreviousWriter)):-
    maze_writer_for(Opciones, PreviousWriter).
maze_writer_for([ ], regular_write).

take(_, [], []).
take(0, _, []).
take(N, [ X | List ], [ X | NewList ]):-
    N > 0,
    NextN is N - 1,
    take(NextN, List, NewList).

write_in_color(Color, Text):-
    ansi_format(Color, Text, []).

with_color(Color, WriteAction):-
    with_output_to(string(S), WriteAction),
    write_in_color(Color, S).

do_times(_, 0):- !.
do_times(Action, Times):- Times > 0, call(Action), NextTimes is Times - 1, do_times(Action, NextTimes).

color_write(ChooseCellColor, Writer, TypeOfWrite, Cell, Position):-
    call(ChooseCellColor, TypeOfWrite, Cell, Position, Color),
    with_color(Color, call(Writer, TypeOfWrite, Cell, Position)).
color_write(ChooseCellColor, Writer, TypeOfWrite, Cell, Position):-
    not(call(ChooseCellColor, TypeOfWrite, Cell, Position, _)),
    call(Writer, TypeOfWrite, Cell, Position).

torcido_write(Writer, maze, "\n", posicion(X, Y)):-
    call(Writer, maze, "\n", posicion(X, Y)), 
    do_times(write(" "), Y),
    !.
torcido_write(Writer, TypeOfWrite, "\n", posicion(X, Y)):-
    TypeOfWrite \= maze,
    call(Writer, TypeOfWrite, "\n", posicion(X, Y)),
    do_times(tty:string_action(nd), Y),
    !.
torcido_write(Writer, TypeOfWrite, Cell, Position):-
    call(Writer, TypeOfWrite, Cell, Position).

regular_write(path, "\n", _):-
    tty:string_action(do),
    !.
regular_write(path, Cell, _):-
    not(path(Cell)),
    tty:string_action(nd),
    tty:string_action(nd),
    !.
regular_write(path, Cell, _):-
    path(Cell),
    write(Cell),
    % !
regular_write(discarded_path, Cell, _):-
    path(Cell),
    write(--),
    !.
regular_write(discarded_path, Cell, _):-
    not(path(Cell)),
    regular_write(path, Cell, _),
    !.
regular_write(maze, .., _):- write("  "), !.
regular_write(maze, Cell, _):- write(Cell), !.

even(N):- mod(N,2) =:= 0.
odd(N):- mod(N,2) =:= 1.

tema3(maze, //, posicion(_, Y), [fg(200, 200, 200)]):- even(Y), !.
tema3(maze, //, posicion(_, Y), [fg(255, 255, 255)]):- odd(Y), !.
tema3(maze, <>, _, [fg(cyan)]):- !.
tema3(path, "-@", _, [fg(yellow)]):- !.
tema3(discarded_path, Cell, _, [fg(80, 80, 80)]):- path(Cell), !.
tema3(path, Cell, _, [fg(red)]):- path(Cell), !.


tema2(maze, //, posicion(_, N), [fg(O, M, P)]):-
    O is max(0, min(255, 255 - N * 3)),
    M is min(255, 150 + N * 3),
    P is 255 - O,
    !.
tema2(maze, <>, _, [fg(cyan)]):- !.
tema2(discarded_path, Cell, _, [fg(80, 80, 80)]):- path(Cell), !.
tema2(path, "-@", posicion(_, N), [fg(M, M, 0)]):- M is min(255, 150 + N * 3), !.
tema2(path, _, _, [fg(cyan)]).

tema1(maze, //, posicion(_, N), [fg(M, 50, O)]):- O is max(0, min(255, 255 - N * 3)), M is min(255, 150 + N * 3), !.
tema1(maze, <>, _, [fg(cyan)]):- !.
tema1(discarded_path, Cell, _, [fg(80, 80, 80)]):- path(Cell), !.
tema1(path, "-@", posicion(_, N), [fg(M, M, 0)]):- M is min(255, 150 + N * 3), !.
tema1(path, Cell, posicion(_, N), [fg(M, 50, 50)]):- path(Cell), M is min(255, 200 + N * 3), !.

%Este metodo dibuja el laberinto dado a la funcion laberinto, laberinto-amplio 
dibujar_laberinto_con(Pos, Migas, Laberinto, Writer, TypeOfWrite, Mutex):-
    % with_mutex(Mutex, write(Mutex)).
    laberinto_con_personaje_y_migas(Laberinto, personaje(Pos, "-@"), Migas, LaberintoDibujable),
    with_mutex(Mutex,
        (
            tty:string_action(ho),
            write_matrix_with(LaberintoDibujable, call(Writer, TypeOfWrite))
        )),
    sleep_after_dibujar(TypeOfWrite).

sleep_after_dibujar(path):- sleep(0.05), !.
sleep_after_dibujar(_).

laberinto_con_personaje_y_migas(Laberinto, personaje(Posicion, Dibujo), [], LaberintoResultado):-
    replace_matrix1(Posicion, Laberinto, Dibujo, LaberintoResultado).
laberinto_con_personaje_y_migas(Laberinto, Personaje, [ miga(Posicion, Direccion) | Migas ], LaberintoResultado):-
    simbolo_direccion(Direccion, Simbolo),
    replace_matrix1(Posicion, Laberinto, Simbolo, L),
    laberinto_con_personaje_y_migas(L, Personaje, Migas, LaberintoResultado).

%Son las guias o cabezaras a la hora de ejecutar resolverlaberinto
simbolo_unico_direccion(arriba, "^").
simbolo_unico_direccion(abajo, "v").
simbolo_unico_direccion(izquierda, ">").
simbolo_unico_direccion(derecha, "<").

simbolo_direccion(arriba, ^^).
simbolo_direccion(abajo, vv).
simbolo_direccion(izquierda, <<).
simbolo_direccion(derecha, >>).